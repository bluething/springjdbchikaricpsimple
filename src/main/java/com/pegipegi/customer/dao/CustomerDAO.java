/**
 * 
 */
package com.pegipegi.customer.dao;

import java.util.List;

import com.pegipegi.customer.model.Customer;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public interface CustomerDAO {
	public void insert(Customer customer);
			
	public Customer findByCustomerId(int custId);
	
	public Customer findByCustomerId2(int custId);

	public List<Customer> findAll();
	
	public List<Customer> findAll2();
	
	public String findCustomerNameById(int custId);
	
	public int findTotalCustomer();
}
