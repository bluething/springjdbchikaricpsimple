/**
 * 
 */
package com.pegipegi.customer.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.pegipegi.customer.dao.CustomerDAO;
import com.pegipegi.customer.model.Customer;
import com.pegipegi.customer.model.CustomerRowMapper;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class JdbcTemplateCustomerDAO implements CustomerDAO {

	private DataSource datasource;
	private CustomerRowMapper customerRowMapper = new CustomerRowMapper();

	/* (non-Javadoc)
	 * @see com.pegipegi.customer.dao.CustomerDAO#insert(com.pegipegi.customer.model.Customer)
	 */
	@Override
	public void insert(Customer customer)
	{
		String sql = "INSERT INTO CUSTOMER (CUST_ID, NAME, AGE) VALUES (?, ?, ?)";

		JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);

		Object[] args = new Object[] {customer.getCustId(), customer.getName(), customer.getAge()};

		int out = jdbcTemplate.update(sql, args);

		if(out != 0) {
			System.out.println("Employee saved with id="+customer.getCustId());
		} else {
			System.out.println("Employee save failed with id="+customer.getCustId());
		}

	}

	/* (non-Javadoc)
	 * @see com.pegipegi.customer.dao.CustomerDAO#findByCustomerId(int)
	 */
	@Override
	public Customer findByCustomerId(int custId)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pegipegi.customer.dao.CustomerDAO#findByCustomerId2(int)
	 */
	@Override
	public Customer findByCustomerId2(int custId)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);
		String sql = "SELECT * FROM CUSTOMER WHERE CUST_ID = ?";
		Customer customer = jdbcTemplate.queryForObject(sql, new Object[]{custId}, customerRowMapper);
		return customer;
	}

	/* (non-Javadoc)
	 * @see com.pegipegi.customer.dao.CustomerDAO#findAll()
	 */
	@Override
	public List<Customer> findAll()
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);
		String sql = "SELECT * FROM CUSTOMER";
		List<Customer> customers = new ArrayList<Customer>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		
		for (Map<String, Object> row : rows) {
			Customer customer = new Customer();
			customer.setCustId((Long)(row.get("CUST_ID")));
			customer.setName((String)row.get("NAME"));
			customer.setAge((Long)row.get("AGE")); //why must long
			customers.add(customer);
		}
			
		return customers;
	}

	/* (non-Javadoc)
	 * @see com.pegipegi.customer.dao.CustomerDAO#findAll2()
	 */
	@Override
	public List<Customer> findAll2()
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);
		String sql = "SELECT * FROM CUSTOMER";
		List<Customer> customers = jdbcTemplate.query(sql, customerRowMapper);
		return customers;
	}

	/* (non-Javadoc)
	 * @see com.pegipegi.customer.dao.CustomerDAO#findCustomerNameById(int)
	 */
	@Override
	public String findCustomerNameById(int custId)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);
		String sql = "SELECT NAME FROM CUSTOMER WHERE CUST_ID = ?";
		String customerName = jdbcTemplate.queryForObject(sql, new Object[]{custId}, String.class);
		return customerName;
	}

	/* (non-Javadoc)
	 * @see com.pegipegi.customer.dao.CustomerDAO#findTotalCustomer()
	 */
	@Override
	public int findTotalCustomer()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setDatasource(DataSource datasource)
	{
		this.datasource = datasource;
	}

}
